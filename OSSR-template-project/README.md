# ESCAPE template metadata
Metadata needed to describe the executable components of the [ESCAPE template project](https://gitlab.in2p3.fr/escape2020/wp3/template_project_escape) project.

* ESCAPE template project
  * https://zenodo.org/record/4923992
  * https://gitlab.in2p3.fr/escape2020/wp3/template_project_escape


