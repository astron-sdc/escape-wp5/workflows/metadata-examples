# Metadata examples
A set of examples for the metadata needed to describe the executable components of OSSR Zenodo entries.

Based on the initial wiki page descriptions
* [Executable-metadata](https://git.astron.nl/groups/astron-sdc/escape-wp5/-/wikis/ESAP/Executable-metadata)
* [Container-metadata](https://git.astron.nl/groups/astron-sdc/escape-wp5/-/wikis/ESAP/Container-metadata)
* [Compute-resources](https://git.astron.nl/groups/astron-sdc/escape-wp5/-/wikis/ESAP/Compute-resource-metadata)
* [Data-resources](https://git.astron.nl/groups/astron-sdc/escape-wp5/-/wikis/ESAP/Data-resource-metadata)

Covering the following examples:

* ESCAPE template project
  * https://zenodo.org/record/4923992
  * https://gitlab.in2p3.fr/escape2020/wp3/template_project_escape

* ESCAPE OSSR library
  * https://zenodo.org/record/5592584
  * https://gitlab.in2p3.fr/escape2020/wp3/eossr

* AMIGA-IAA hcg-16
  * https://zenodo.org/record/5534682
  * https://github.com/AMIGA-IAA/hcg-16

More examples will come from the [ESCAPE OSSR onboarding catalog](https://escape2020.pages.in2p3.fr/wp3/ossr-pages/page/escape_ossr/onboarding/)

