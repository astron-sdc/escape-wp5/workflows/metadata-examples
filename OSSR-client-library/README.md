# ESCAPE OSSR library metadata
Metadata needed to describe the executable components of the [ESCAPE OSSR library](https://gitlab.in2p3.fr/escape2020/wp3/eossr) project.

* ESCAPE OSSR library
  * https://zenodo.org/record/5592584
  * https://gitlab.in2p3.fr/escape2020/wp3/eossr


