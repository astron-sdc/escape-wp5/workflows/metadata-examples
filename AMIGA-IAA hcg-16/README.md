# AMIGA-IAA hcg-16 metadata
Metadata needed to describe the executable components of the [AMIGA-IAA hcg-16](https://github.com/AMIGA-IAA/hcg-16) project.

* AMIGA-IAA hcg-16
  * https://zenodo.org/record/5534682
  * https://github.com/AMIGA-IAA/hcg-16

